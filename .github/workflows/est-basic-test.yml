name: Basic EST

on:
  workflow_call:
    inputs:
      db-image:
        required: false
        type: string

jobs:
  # docs/installation/est/Installing_EST.md
  test:
    name: Test
    runs-on: ubuntu-latest
    env:
      SHARED: /tmp/workdir/pki
    steps:
      - name: Clone repository
        uses: actions/checkout@v3

      - name: Retrieve PKI images
        uses: actions/cache@v3
        with:
          key: pki-images-${{ github.sha }}
          path: pki-images.tar

      - name: Load PKI images
        run: docker load --input pki-images.tar

      - name: Create network
        run: docker network create example

      - name: Set up DS container
        run: |
          tests/bin/ds-container-create.sh ds
        env:
          IMAGE: ${{ inputs.db-image }}
          HOSTNAME: ds.example.com
          PASSWORD: Secret.123

      - name: Connect DS container to network
        run: docker network connect example ds --alias ds.example.com

      - name: Set up PKI container
        run: |
          tests/bin/runner-init.sh pki
        env:
          HOSTNAME: pki.example.com

      - name: Connect PKI container to network
        run: docker network connect example pki --alias pki.example.com

      - name: Install CA
        run: |
          docker exec pki pkispawn \
              -f /usr/share/pki/server/examples/installation/ca.cfg \
              -s CA \
              -D pki_ds_hostname=ds.example.com \
              -D pki_ds_ldap_port=3389 \
              -v
          docker exec pki pki-server cert-find

      - name: Configure admin certificate
        run: |
          docker exec pki pki-server cert-export ca_signing --cert-file ca_signing.crt
          docker exec pki pki client-cert-import ca_signing --ca-cert ca_signing.crt
          docker exec pki pki pkcs12-import \
              --pkcs12 /root/.dogtag/pki-tomcat/ca_admin_cert.p12 \
              --pkcs12-password Secret.123

      - name: Create EST group and user
        run: |
          docker exec pki pki -n caadmin ca-group-add "EST RA Agents"
          docker exec pki pki -n caadmin ca-user-add est-ra-1 --fullName "EST RA 1" --password est4ever
          docker exec pki pki -n caadmin ca-group-member-add "EST RA Agents" est-ra-1

      - name: Enable EST profile 
        run: |
          docker exec pki pki -n caadmin ca-profile-add --raw /usr/share/pki/ca/profiles/ca/estServiceCert.cfg
          docker exec pki pki -n caadmin ca-profile-enable estServiceCert

      - name: Create  EST subsystem
        run: |
          docker exec pki pki-server est-create

      - name: Configure EST backend
        run: |
          cat >backend.conf <<EOF
          class=org.dogtagpki.est.DogtagRABackend
          url=https://pki.example.com:8443
          profile=estServiceCert
          username=est-ra-1
          password=est4ever
          EOF
          docker cp backend.conf pki:/etc/pki/pki-tomcat/est/backend.conf

      - name: Configure EST authorization
        run: |
          cat >authorizer.conf <<EOF
          class=org.dogtagpki.est.ExternalProcessRequestAuthorizer
          executable=/usr/local/libexec/estauthz
          EOF
          docker cp authorizer.conf pki:/etc/pki/pki-tomcat/est/authorizer.conf
          cat >estauthz <<EOF
          #!/usr/bin/python3
          import json, sys
          ALLOWED_ROLE = 'estclient'
          obj = json.loads(sys.stdin.read())
          if not ALLOWED_ROLE in obj['authzData']['principal']['roles']:
              print(f'Principal does not have required role {ALLOWED_ROLE!r}')
              sys.exit(1)
          EOF
          docker cp estauthz pki:/usr/local/libexec/estauthz
          docker exec pki chmod +x /usr/local/libexec/estauthz
                    
      - name: Configure EST authentication
        run: |
          docker exec pki pki-server est-deploy
          cat >realm.conf <<EOF
          class=com.netscape.cms.realm.PKIInMemoryRealm
          username=alice
          password=4me2Test
          roles=estclient
          EOF
          docker cp realm.conf pki:/etc/pki/pki-tomcat/est/realm.conf
          docker exec pki pki-server restart --wait

      - name: Set up client container
        run: |
          docker run \
              --name client \
              --hostname client.example.com \
              --network example \
              --network-alias client.example.com \
              -it \
              --detach \
              quay.io/dogtagpki/libest

      - name: Get CA certificate
        run: |
          docker exec client curl -o cacert.p7 -k https://pki.example.com:8443/.well-known/est/cacerts
          docker exec client openssl base64 -d --in cacert.p7 --out cacert.p7.der
          docker exec client openssl pkcs7 --in cacert.p7.der -inform DER -print_certs -out cacert.pem
          docker exec client openssl x509 -in cacert.pem -text -noout

          echo "subject=O = EXAMPLE, OU = pki-tomcat, CN = CA Signing Certificate" > expected
          docker exec client openssl x509 -in cacert.pem -noout -subject | tee actual
          diff actual expected

      - name: Enroll certificate
        run: |
          docker exec -e EST_OPENSSL_CACERT=cacert.pem client estclient -e -s pki.example.com -p 8443 --common-name client.example.com -o . -u alice -h 4me2Test
          docker exec client openssl base64 -d --in cert-0-0.pkcs7 --out cert-0-0.pkcs7.der
          docker exec client openssl pkcs7 -in cert-0-0.pkcs7.der -inform DER -print_certs -out cert.pem
          docker exec client openssl x509 -in cert.pem -text -noout

          docker exec client openssl x509 -in cert.pem -noout -subject -issuer | tee actual
          echo "subject=CN = client.example.com" > expected
          echo "issuer=O = EXAMPLE, OU = pki-tomcat, CN = CA Signing Certificate" >> expected
          diff expected actual





          
