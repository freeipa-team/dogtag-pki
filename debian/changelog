dogtag-pki (11.4.3-1) UNRELEASED; urgency=medium

  * New upstream release.
  * patches: Refreshed.
  * patches: Drop an upstreamed patch.
  * rules: Drop setting nssdb type, the default is sql now.
  * Add pki-est.
  * install: Updated.
  * control: Bump depends on jss, tomcatjss, ldapjdk.
  * control: Drop python3-distutils from build-depends. (Closes:
    #1065850)
  * patches, rules: Instead of shipping own service files, patch the
    upstream ones and install them under usr/lib. (Closes: #1054480)
  * control: Add dh-sequence-movetousr to build-depends.
  * control: Add python3-six to build-depends.

 -- Timo Aaltonen <tjaalton@debian.org>  Tue, 07 Feb 2023 10:55:08 +0200

dogtag-pki (11.2.1-2) unstable; urgency=medium

  * Upload to sid.

 -- Timo Aaltonen <tjaalton@debian.org>  Fri, 10 Feb 2023 08:58:51 +0200

dogtag-pki (11.2.1-1) experimental; urgency=medium

  * New upstream release.
  * patches: Drop obsolete patches, refresh others.
  * control: Remove obsolete (build-)deps, xalan2, xerces, commons-
    resolver.
  * control: Symkey got removed, drop the packages.
  * fix-resteasy-path.diff: Fix path to resteasy jars.
  * rules: Drop obsolete build options.
  * rules: Rename BUILD_PKI_CONSOLE option to WITH_CONSOLE.
  * rules: Drop jboss-logging link creation, handled by cmake now.
  * patches, rules: Fix creating links to annotations jar.
  * rules: Rename WITH_TEST to RUN_TESTS.
  * control: Fold tps-client in pki-tools.
  * copyright: Updated for tps-client removals.
  * patches: Force python dist-packages path.
  * install: Updated.
  * control: Drop unused (build-)deps, libjaxp1.3-java, policycoreutils,
    python3-nss. Mark python3-dev as <!nocheck>. (Closes: #1010636)

 -- Timo Aaltonen <tjaalton@debian.org>  Tue, 07 Feb 2023 10:23:58 +0200

dogtag-pki (11.0.6-2) unstable; urgency=medium

  * tests: Support only amd64. (Closes: #1013935)

 -- Timo Aaltonen <tjaalton@debian.org>  Fri, 03 Feb 2023 18:25:25 +0200

dogtag-pki (11.0.6-1) unstable; urgency=medium

  * New upstream release.
    - CVE-2022-2414 (Closes: #1014957)
  * control: Fix pki-base-java to depend on default-jre-headless instead
    of a versioned one, it shouldn't be necessary to hardcode it
    anymore. (Closes: #1024462)
  * control: Bump dependency on jss, ldapjdk, tomcatjss.
  * dont-use-deprecated-python-ldap-options.diff: Drop constants removed
    in python-ldap 3.4.
  * rules: Bump JAVA_HOME to match java 17.

 -- Timo Aaltonen <tjaalton@debian.org>  Thu, 12 Jan 2023 20:49:28 +0200

dogtag-pki (11.0.3-4) unstable; urgency=medium

  * Fix tab/spaces in patch.

 -- Timo Aaltonen <tjaalton@debian.org>  Thu, 17 Mar 2022 21:06:11 +0200

dogtag-pki (11.0.3-3) unstable; urgency=medium

  * force-request-timeout-default.diff: Another attempt to set
    request_timeout. (Closes: #1001801)

 -- Timo Aaltonen <tjaalton@debian.org>  Thu, 17 Mar 2022 14:10:45 +0200

dogtag-pki (11.0.3-2) unstable; urgency=medium

  * default-webapp-timeout.diff: Dropped, instead set
    pki_status_request_timeout in the autopkgtest config. (Closes:
    #1001801)
  * tests: Dump tomcat log on failure.

 -- Timo Aaltonen <tjaalton@debian.org>  Wed, 16 Mar 2022 18:53:28 +0200

dogtag-pki (11.0.3-1) unstable; urgency=medium

  * New upstream release.
  * defauilt-webapp-timeout.diff: Force default webapp start timeout to
    120 seconds. (Closes: #1001801)
  * tests: Dump selftests.log if pkispawn fails, and also drop dumping
    pkispawn log since we have --debug now.

 -- Timo Aaltonen <tjaalton@debian.org>  Tue, 15 Mar 2022 13:21:56 +0200

dogtag-pki (11.0.0-1) unstable; urgency=medium

  * New upstream release
    - CVE-2020-25715 (Closes: #988153)
  * dont-install-deleted-files.diff: Dropped, obsolete.
  * patches: Refreshed.
  * rules: Bump tomcat source path.
  * add-freebl-headers.diff: Add headers that libnss3-dev doesn't ship.
  * pki-tools: Add p12tool.
  * tests: Add --debug to pkispawn/pkidestroy.
  * Update service files.
  * control: Drop python3-pytest-runner from build-deps, not used anymore.
  * use-resteasy-legacy.diff: Dropped, obsolete and not applied anyway.
  * control: Drop unused libcommons-collections3-java from server depends.
  * fix-tomcat-paths.diff: Add catalina.properties and pki.policy to list of files to fix.
  * fix-symkey-path.diff: Refreshed.
  * control: Bump depends on libjss-java, libldap-java, libtomcatjss-java, libidm-console-framework-java.
  * install: Fix javadoc install.

 -- Timo Aaltonen <tjaalton@debian.org>  Tue, 19 Oct 2021 18:25:25 +0300

dogtag-pki (10.10.6-1) unstable; urgency=medium

  * New upstream release. (Closes: #993534)
    - CVE-2021-3551 (Closes: #991665)
  * CVE-2021-20179.diff: Dropped, upstream.
  * Refresh patches.
  * JNA requirement got dropped, update packaging for it.
  * pki-server.postinst: Remove world access from existing installation
    logs.
  * control: Drop velocity from depends, it's not used anymore.
  * control: Drop libcommons-httpclient-java from depends, not used
    anymore.
  * control: Bump libjss dependency.
  * pki-tps.install: Updated.
  * tests: Add iproute2 to test depends. (Closes: #991173)
  * tests: Add isolation-container to test restrictions. (Closes:
    #991174)

 -- Timo Aaltonen <tjaalton@debian.org>  Tue, 07 Sep 2021 14:25:32 +0300

dogtag-pki (10.10.2-3) unstable; urgency=medium

  * Add python3-ldap to python3-pki-base Depends. Thanks, Francisco
    Vilmar Cardoso Ruviaro and Rene Luria! (Closes: #985340)
  * Move p11-kit-trust.so to pki-tools, add p11-kit-modules to pki-tools
    Depends. (Closes: #986080)

 -- Timo Aaltonen <tjaalton@debian.org>  Wed, 14 Apr 2021 11:02:32 +0300

dogtag-pki (10.10.2-2) unstable; urgency=medium

  * CVE-2021-20179.diff: Fix renewal profile approval process.

 -- Timo Aaltonen <tjaalton@debian.org>  Fri, 12 Mar 2021 16:29:17 +0200

dogtag-pki (10.10.2-1) unstable; urgency=medium

  * New upstream release.
  * fix-tomcat-paths.diff: Fix some dangling symlinks to point to the
    tomcat9/skel/conf files.
  * control: Add dependencies on openssl.
  * control: Add libjaxp1.3-java to build-depends and pki-base-java
    depends.
  * control, fix-tomcat-jars.diff: Updated to use correct servlet.jar,
    drop libservlet3.1-java from dependencies.
  * control: Move pki-tools to pki-base-java Suggests to avoid a
    circular dependency. (Closes: #977418)
  * use-bash.diff: Force bash only for pkidaemon, where it's actually
    needed, and fix syntax for scripts/config. (Closes: #963049)

 -- Timo Aaltonen <tjaalton@debian.org>  Wed, 16 Dec 2020 15:56:20 +0200

dogtag-pki (10.10.1-1) unstable; urgency=medium

  * New upstream release.
  * control: Pki-tools should depend on pki-base-java instead of just
    pki-base.
  * fix-runuser-path.diff: Fix path to /sbin/runuser.
  * control: Add python3-pki-base to pki-tools depends.
  * control: Bump libtomcatjss-java dependencies.
  * control: Add python3-ldap to build-depends.

 -- Timo Aaltonen <tjaalton@debian.org>  Sun, 06 Dec 2020 10:12:21 +0200

dogtag-pki (10.10.0-1) unstable; urgency=medium

  * New upstream release.
  * patches: Refreshed, fix-java11-dependencies.diff and fix-pki-java-
    path.diff dropped, upstream.
  * control: Bump jss and tomcatjss dependencies.
  * control: Migrate to libcommons-lang3-java.
  * control, pki-server.install, rules: Add support for systemd
    notifications, add libjna-java to depends.
  * pki-tools, copyright: Java and native-tools got moved, update paths.

 -- Timo Aaltonen <tjaalton@debian.org>  Mon, 02 Nov 2020 09:23:07 +0200

dogtag-pki (10.9.4-1) unstable; urgency=medium

  * New upstream release.
  * control: Fix pki-base-java openjdk depends, bump it to 11.
  * rules: Set P11_KIT_TRUST.
  * add-more-deps.diff: Dropped, upstream.
  * fix-java11-dependencies.diff: Make sure the necessary directories
    are created before adding symlinks to jars.
  * rules: Add more cruft to remove on dh_auto_clean.
  * revert-support-jdk8-jdk11-rpm-builds.diff: Dropped, fix PKI_JAVA_PATH
    instead.
  * Add pki.conf to service file environment.
  * control: Bump dependency on libjss-java.

 -- Timo Aaltonen <tjaalton@debian.org>  Thu, 17 Sep 2020 11:45:27 +0300

dogtag-pki (10.9.2-1) unstable; urgency=medium

  * New upstream release.
  * control, add-more-deps.diff: Add jaxb-impl and javax.activation to server
    dependencies due to jdk11.
  * source: Add some doc images to extend-diff-ignore.

 -- Timo Aaltonen <tjaalton@debian.org>  Mon, 24 Aug 2020 23:32:11 +0300

dogtag-pki (10.9.1-2) unstable; urgency=medium

  * pki-base.postinst: Drop --silent. (Closes: #963048)

 -- Timo Aaltonen <tjaalton@debian.org>  Fri, 14 Aug 2020 12:43:32 +0300

dogtag-pki (10.9.1-1) unstable; urgency=medium

  * New upstream release.
  * fix-javadoc-build.diff: Dropped, upstream.
  * server.install: Updated.
  * debian-support.diff: Fix more hardcodings of /etc/sysconfig.
  * patches: Refreshed.
  * rules: Set PKI_JAVA_PATH.
  * rules: Drop obsolete WITH_PYTHON options.
  * revert-support-jdk8-jdk11-rpm-builds.diff: This commit breaks the java path.
  * ci: Disable reprotest, it gets stuck and times out.
  * create-target-wants.diff: Add an entry to the log if this is
    triggered.
  * Add a debianized service file for pki-tomcatd-nuxwdog.

 -- Timo Aaltonen <tjaalton@debian.org>  Fri, 14 Aug 2020 09:41:10 +0300

dogtag-pki (10.9.0~a2-2) experimental; urgency=medium

  * server.install: Updated.

 -- Timo Aaltonen <tjaalton@debian.org>  Wed, 17 Jun 2020 14:04:35 +0300

dogtag-pki (10.9.0~a2-1) experimental; urgency=medium

  * New upstream prerelease.
  * rules: Fix java version in pki.conf.
  * Drop pki-server upgrade from postinst, and drop --validate option
    from the systemd service as it's gone.
  * patches: Refreshed.

 -- Timo Aaltonen <tjaalton@debian.org>  Tue, 16 Jun 2020 20:34:07 +0300

dogtag-pki (10.9.0~a1-1) experimental; urgency=medium

  * New upstream prerelease.
  * watch: Updated.
  * patches: Refreshed.
  * control, rules: Build with default-jdk, bump jss, tomcatjss
    dependencies. (Closes: #920725, #921926)
  * control: Add libcommons-net-java to depends.
  * fix-javadoc-build.diff: Fix building the javadoc.
  * install: Updated, nsutil got folded in cmsutil.
  * copyright: Don't exclude the fonts.

 -- Timo Aaltonen <tjaalton@debian.org>  Thu, 28 May 2020 02:16:42 +0300

dogtag-pki (10.8.3-3) unstable; urgency=medium

  * use-bash.diff: Added all scripts that source scripts/config.
  * control: Add libstax-java to pki-base-java depends in order to
    silence noise from the jar scanner.

 -- Timo Aaltonen <tjaalton@debian.org>  Fri, 10 Apr 2020 11:47:09 +0300

dogtag-pki (10.8.3-2) unstable; urgency=medium

  * control: Mark themes and javadoc M-A: foreign.
  * control: Bump policy to 4.5.0.
  * control: Add python3-pytest-runner as a build-dep.
  * fix-tomcat-paths.diff: Use /usr/share/tomcat9/bin/version.sh when
    determining the tomcat version.
  * control: Add python3-ipahealthcheck-core to pki-server depends.

 -- Timo Aaltonen <tjaalton@debian.org>  Mon, 30 Mar 2020 14:52:56 +0300

dogtag-pki (10.8.3-1) unstable; urgency=medium

  * New upstream release.
  * patches: Refreshed.
  * control: Add python3-setuptools to build-depends.
  * fix-healthcheck-install.diff: Use debian layout when installing the
    healthcheck stuff.
  * install: Updated.
  * Add debian/gitlab-ci.yml.
  * gbp.conf: Update for the experimental branch.
  * tests: Add sudo to Depends.

 -- Timo Aaltonen <tjaalton@debian.org>  Wed, 18 Mar 2020 00:41:13 +0200

dogtag-pki (10.7.4-2) unstable; urgency=medium

  * Use pybuild.

 -- Timo Aaltonen <tjaalton@debian.org>  Sat, 08 Feb 2020 17:35:13 +0200

dogtag-pki (10.7.4-1) unstable; urgency=medium

  * New upstream release.
  * tests: Ignore pkidestroy failures.
  * Use debhelper-compat 12.

 -- Timo Aaltonen <tjaalton@debian.org>  Thu, 06 Feb 2020 21:13:33 +0200

dogtag-pki (10.7.3-4) unstable; urgency=medium

  * tomcat-start.sh: Dropped everything we don't need from the original
    copy from tomcat9.
  * debian-support.diff: Drop the hunk about disabling
    pki_security_manager, it works fine with defaults.
  * control: Bump pki-base-java dep on libjss-java.
  * fix-tomcat-paths.diff: Cleanups.
  * tests: Redirect dscreate stderr to stdout.
  * control: Drop dependency on pki-base from python3-pki-base. (Closes:
    #940287)

 -- Timo Aaltonen <tjaalton@debian.org>  Tue, 17 Sep 2019 18:22:15 +0300

dogtag-pki (10.7.3-3) unstable; urgency=medium

  * fix-tomcat-paths.diff: We have /etc/default/tomcat9 instead of
    tomcat.conf.
  * pki-tomcatd@.service: Updated to match the upstream version.
  * hardcode-tomcat-version.diff: Dropped, instead pass --tomcat  for
    pki-server migrate in the service file.

 -- Timo Aaltonen <tjaalton@debian.org>  Sat, 14 Sep 2019 00:06:00 +0300

dogtag-pki (10.7.3-2) unstable; urgency=medium

  * Switch to python3. (Closes: #918538, #936435, #935606)
  * tests: Migrate to dscreate, bump 389-ds-base dependency.

 -- Timo Aaltonen <tjaalton@debian.org>  Wed, 11 Sep 2019 23:40:05 +0300

dogtag-pki (10.7.3-1) experimental; urgency=medium

  * New upstream release.
  * rules: Fix arch:all build.
  * patches: Refreshed, use-new-pkcs11-interface.diff dropped.
  * fix-hamcrest-jar.diff: Fix path to hamcrest jar.
  * pki-tools.install: Updated.
  * rules: Disable Junit tests for now.
  * control: Add go-md2man to build-depends.
  * control: Bump dependency on libldap-java.
  * control: Bump dependency on libjss-java.
  * control: Bump dependency on libtomcatjss-java.
  * server.postinst: Use 'pki-server migrate'.
  * control, rules: Drop obsolete dependencies libjavassist-java,
    libjaxrs-api-java.
  * control: Add keyutils to pki-server depends.

 -- Timo Aaltonen <tjaalton@debian.org>  Fri, 09 Aug 2019 23:30:54 +0300

dogtag-pki (10.6.10-2) unstable; urgency=medium

  * rules: Fix arch:all build.

 -- Timo Aaltonen <tjaalton@debian.org>  Mon, 29 Jul 2019 14:09:25 +0300

dogtag-pki (10.6.10-1) unstable; urgency=medium

  * Upload to unstable.

 -- Timo Aaltonen <tjaalton@debian.org>  Fri, 19 Jul 2019 10:09:06 +0300

dogtag-pki (10.6.10-0ubuntu2) eoan; urgency=medium

  * tests: Don't test TPS, pkispawn fails for unknown reasons.

 -- Timo Aaltonen <tjaalton@debian.org>  Thu, 09 May 2019 09:27:41 +0300

dogtag-pki (10.6.10-0ubuntu1) eoan; urgency=medium

  * New upstream release.
    - debian-support.diff: Updated
    - run-pki-server-migrate-on-start.diff: Dropped, obsolete
  * hardcode-tomcat-version.diff: Use a real version, not a wildcard.
  * debian-support.diff: Fix a typo with deployment_root.
  * Remove the initscript, add a proper systemd service.
  * control: Drop libnuxwdog-java from depends, nuxwdog merged to dogtag.
  * pki-server.install: Update pki-server-nuxwdog install path.
  * control, rules: Use JDK8 again.
  * pki-tools.install: Updated.
  * control: Bump build-dep on libjss-java.
  * control: Bump dependencies on libtomcatjss-java.

 -- Timo Aaltonen <tjaalton@debian.org>  Fri, 26 Apr 2019 13:33:51 +0300

dogtag-pki (10.6.8-2) unstable; urgency=medium

  * control: Replace libsrvcore-dev build-dep with 389-ds-base-dev.
  * Migrate to tomcat9.

 -- Timo Aaltonen <tjaalton@debian.org>  Fri, 07 Dec 2018 10:55:22 +0200

dogtag-pki (10.6.8-1) unstable; urgency=medium

  * New upstream release.
  * control: Bump depends on nuxwdog and tomcatjss.
  * control, use-new-pkcs11-interface.diff: Bump libjss-java depends to
    4.5.1, fix build against jdk9+. (Closes: #893142)
  * patches: Refreshed.
  * control: Add libjackson2-core-java, -databind-java to build-depends.
  * dogtag-pki-server-theme.install: Updated.
  * control, rules: Build-depend on default-jdk again, set JAVA_HOME to
    match.
  * tests: Force C locale so that error messages from python can be shown.
  * control: Bump libresteasy3.0-java dependency for jackson2 provider.
  * control, rules: Replace libjboss-annotations-1.2-api-java with
    libgeronimo-annotation-1.3-spec-java. Drop dependency on
    libscannotation-java.

 -- Timo Aaltonen <tjaalton@debian.org>  Wed, 05 Dec 2018 22:21:50 +0200

dogtag-pki (10.6.7-1) unstable; urgency=medium

  * New upstream release.
  * server.postinst: Server migration has been moved to the systemd
    unit/initfile, drop it from here.

 -- Timo Aaltonen <tjaalton@debian.org>  Tue, 09 Oct 2018 22:26:07 +0300

dogtag-pki (10.6.6-2) unstable; urgency=medium

  * hardcode-tomcat-version.diff: Our tomcat doesn't have a script to
    query the version, so hardcode it here so that 'pki-server migrate'
    works.
  * run-pki-server-migrate-on-start.diff: Run 'pki-server migrate' on
    startup to match what the systemd service does.

 -- Timo Aaltonen <tjaalton@debian.org>  Mon, 27 Aug 2018 20:30:44 +0300

dogtag-pki (10.6.6-1) unstable; urgency=medium

  * New upstream release.
  * tests: Fix the test loop.
  * control, rules: Add libjboss-annotations-1.2-api-java to pki-server
    depends, add links to lib directories.
  * watch: Updated.
  * copyright: Update excluded files.
  * debian-support.diff: Refreshed.
  * pki-*.install: Updated.
  * rules: Updated cmake variables for default nssdb and theme.
  * control: Bump {build-}depends on libjss-java, libldap-java,
    libtomcatjss-java and libidm-console-framework-java.
  * rules: Remove tomcat/ on clean.

 -- Timo Aaltonen <tjaalton@debian.org>  Thu, 23 Aug 2018 09:12:47 +0300

dogtag-pki (10.6.1-1) experimental; urgency=medium

  * New upstream release.
    - drop cve fix, applied upstream
  * control: Add conflicts on libtomcat7-java to pki-server.
  * rules: Replace setting DEB_BUILD_ARCH with including
    architecture.mk.
  * control: Update maintainer address.
  * Bump debhelper to 11.
  * control: Bump policy to 4.1.4.
  * control: Update dogtag-pki description to mention that it's a
    metapackage.
  * control: Add pki-tools to pki-base-java depends. (Closes: #891370)
  * tests: Improve logging, fail properly.

 -- Timo Aaltonen <tjaalton@debian.org>  Sun, 20 May 2018 14:47:13 +0300

dogtag-pki (10.6.0-2) experimental; urgency=medium

  * rules: Build everything in one pass.
  * Fix ACL evaluation in allow,deny mode. (Closes: #893690)
    - CVE-2018-1080

 -- Timo Aaltonen <tjaalton@debian.org>  Wed, 18 Apr 2018 15:07:20 +0300

dogtag-pki (10.6.0-1) experimental; urgency=medium

  * New upstream release.
  * control: Update VCS urls.

 -- Timo Aaltonen <tjaalton@debian.org>  Wed, 18 Apr 2018 00:22:25 +0300

dogtag-pki (10.6.0~beta2-3) experimental; urgency=medium

  * rules: Fix JAVA_HOME, create a symlink to the native jvm dir and
    ship it with pki-server.
  * pki-base.postinst: Modify JAVA_HOME for installed instances on
    upgrade.
  * debian-support.diff: Revert start delay to 5s, use systemctl
    enable/disable.

 -- Timo Aaltonen <tjaalton@debian.org>  Tue, 10 Apr 2018 13:20:34 +0300

dogtag-pki (10.6.0~beta2-2) experimental; urgency=medium

  * pki-tools: Add new manpages.
  * debian-support.diff: Fix keystore permissions.
  * debian-support.diff: Skip systemctl enable/disable.
  * control: Add openjdk-8-jre-headless to pki-base-java depends.

 -- Timo Aaltonen <tjaalton@debian.org>  Fri, 06 Apr 2018 16:11:04 +0300

dogtag-pki (10.6.0~beta2-1) experimental; urgency=medium

  * New upstream prerelease.
  * patches: Refreshed.
  * control, rules: Build using tomcat 8.5, adjust dependencies to
    match.
  * fix-jar-search.diff: Dropped, upstream
  * rules: Use sql nssdb's by default.
  * debian-support.diff: Bump the delay after starting the instance to 10s.
  * fix-symkey-path.diff: Move symkey.jar handling here from fix-jar-
    search.diff.
  * fix-tomcat-paths.diff: Use tomcat paths provided by Debian,
    merge fix-cli-migrate.diff here.
  * tools: DRMTool links are handled by cmake now, drop .links.
  * rules: Don't clean usr/share/pki/server/lib before dh_install, it's
    jar symlinks now.
  * base.install: Updated.
  * control, rules: Build using JDK8.
  * control: Add python3-distutils to build-depends.
  * rename-logging-config.diff: Dropped, upstream.
  * use-bindsto.diff: Dropped, upstream.
  * fix-cve-2016-1240.diff: Dropped, upstream.
  * debian-support.diff: Tomcat setup upstreamed.

 -- Timo Aaltonen <tjaalton@debian.org>  Fri, 30 Mar 2018 09:18:41 +0300

dogtag-pki (10.5.5-1) unstable; urgency=medium

  * New upstream release.
  * tests: Add some debugging info, and force the hostname if it isn't
    set.
  * patches: Refreshed.
  * tools.install: Updated.
  * tests: Remove installed instances.

 -- Timo Aaltonen <tjaalton@debian.org>  Fri, 09 Feb 2018 16:29:06 +0200

dogtag-pki (10.5.3-4) unstable; urgency=medium

  * rules: Remove resteasy-jandex-jaxrs.jar symlink, it looks unused
    anyway. (Closes: #857150)
  * tests: Sleep for 10 seconds between spawning instances, it seems
    racy otherwise.

 -- Timo Aaltonen <tjaalton@debian.org>  Fri, 05 Jan 2018 10:36:19 +0200

dogtag-pki (10.5.3-3) unstable; urgency=medium

  * control: Add libhttpclient-java, libhttpcore-java, libjaxrs-api-java to
    pki-base-java Depends.
  * rename-logging-config.diff: Rename LOGGING_CONFIG to
    PKI_LOGGING_CONFIG, otherwise catalina startup would fail.
  * fix-jar-search.diff: Fix search for commons-collections3.jar and
    symkey.jar.
  * rules: Link jboss-logging.jar under server/common/lib too.

 -- Timo Aaltonen <tjaalton@debian.org>  Fri, 22 Dec 2017 14:32:32 +0200

dogtag-pki (10.5.3-2) unstable; urgency=medium

  * control: Add python{,3}-cryptography to pki-base, pki-server and
    python3-pki-base Depends.

 -- Timo Aaltonen <tjaalton@debian.org>  Fri, 22 Dec 2017 00:52:21 +0200

dogtag-pki (10.5.3-1) unstable; urgency=medium

  * New upstream release.
  * watch: Updated, upstream provides proper tags now.
  * copyright: Add Files-Excluded for tarball rebuild.
  * patches: Drop fix-CVE-2017-7537.diff, refresh others.
  * use-usr-bin.diff: Replace with an upstreamed patch.
  * Drop fix-junit-jar.diff, add fix-jar-search.diff and modify debian-
    support.diff and rules to not hardcode distro-specific jar names.
  * control: Add libslf4j-java, libhttpclient-java, libhttpcore-java to
    build-depends/depends.
  * control: Bump dependency on libjss-java.
  * rules: Use dh_missing, and drop creating links under subsys dirs as
    that is handled by CMake now.
  * *install: Added new files to -base, -server and -tools.
  * control: Bump dependency on libldap-java.
  * use-bindsto.diff: Fix pki-tpsd.service to use BindsTo instead of
    BindTo. (Closes: #857186)

 -- Timo Aaltonen <tjaalton@debian.org>  Thu, 21 Dec 2017 18:11:15 +0200

dogtag-pki (10.3.5+12-6) unstable; urgency=medium

  * Fix autopkgtest to be robust in the face of changed iproute2 output.

 -- Timo Aaltonen <tjaalton@debian.org>  Thu, 21 Dec 2017 10:59:59 +0200

dogtag-pki (10.3.5+12-5) unstable; urgency=medium

  * rules: Add a link to jboss-logging.jar.
  * pki-base, pki-server: Fix postinst, strip cruft from the version string.
  * control: Use tomcat8.0. (Closes: #823332, #846714)
  * control: Add libcommons-httpclient-java to build-depends, and
    pki-base-java depends.
  * control: Use resteasy3.0.
  * fix-CVE-2017-7537.diff: Change defaults for cmc plugin. (Closes:
    #869261)
  * control: Bump dependency on libtomcajss-java to verify we have the
    correct build.

 -- Timo Aaltonen <tjaalton@debian.org>  Sat, 21 Oct 2017 11:58:04 +0300

dogtag-pki (10.3.5+12-4) unstable; urgency=medium

  * pki-tomcatd.init: If no instance is configured, the initscript
    machinery would return error value 5 or 6. This messes up systemd, so
    just use 'exit 1' on every non-zero return value. (LP: #1664453)
  * pki-server.postinst: Clarify pki-tomcatd initial start failure
    message a bit.
  * Depend libresteasy-java << 3.1.0, because the new on doesn't work
    even after fixing the build.
  * pki-tools.links: Fix the convenience links DRMTool -> KRATool.
    (Closes: #857148)
  * pki-base.postinst: Force recreating pki.version if upgrading from
    older than 10.3.5-1. (LP: #1691655)

 -- Timo Aaltonen <tjaalton@debian.org>  Thu, 18 May 2017 09:10:17 +0300

dogtag-pki (10.3.5+12-3) unstable; urgency=medium

  * server.postrm: Remove /etc/default/pki-tomcat on purge.
  * create-target-wants.diff: Create target.wants-directories in the
    python deployment scriptlet too.

 -- Timo Aaltonen <tjaalton@debian.org>  Thu, 16 Feb 2017 11:09:38 +0200

dogtag-pki (10.3.5+12-2) unstable; urgency=medium

  * base, server, tools: Add new manpages.
  * use-resteasy-legacy.diff: Fix javadoc build.

 -- Timo Aaltonen <tjaalton@debian.org>  Wed, 15 Feb 2017 13:24:15 +0200

dogtag-pki (10.3.5+12-1) unstable; urgency=medium

  * New upstream snapshot. Version number is derived from the Fedora
    release, 10.3.5+12 maps to pki-core 10.3.5-12 on Fedora.
  * sync-rpm-10.3.5-7.diff: Dropped.
  * use-resteasy-legacy.diff, control: Port to resteasy 3.1.0-2 which
    ships resteasy-legacy.jar. (LP: #1664457)

 -- Timo Aaltonen <tjaalton@debian.org>  Wed, 15 Feb 2017 11:06:47 +0200

dogtag-pki (10.3.5-7) unstable; urgency=medium

  * debian-support: Fix an upgrade script to use /etc/default instead of
    /etc/sysconfig.
  * debian-support: Fix nuxwdog to use /etc/default in each case.
  * create-target-wants.diff: Create the systemd target.wants
    directories on demand.
  * pki-server.dirs: Drop target.wants-directories, they'd just get
    deleted by systemd helpers and are now created on demand anyway.

 -- Timo Aaltonen <tjaalton@debian.org>  Wed, 18 Jan 2017 04:20:42 +0200

dogtag-pki (10.3.5-6) unstable; urgency=medium

  * sync-rpm-10.3.5-7.diff: Pull changes from upstream branch needed by
    newer freeipa.
  * tools.install: Add CMCEnroll manpage.

 -- Timo Aaltonen <tjaalton@debian.org>  Thu, 01 Dec 2016 10:08:50 +0200

dogtag-pki (10.3.5-5) unstable; urgency=medium

  * server: Add /etc/dogtag to dirs, clean up stuff created by pkispawn
    on purge.
  * control: Add libscannotation-java to server depends.
  * use-bash.diff: Revert some of 4708983b8 to use bash instead of sh in
    some places where checkbashisms reported warnings.
  * fix-cve-2016-1240.diff: Fix CVE-2016-1240 in scripts/operations
    which has code copied from the tomcat initscript.
  * pki-{ca,kra,ocsp,tks,tps}.postrm: Remove logfiles on purge.

 -- Timo Aaltonen <tjaalton@debian.org>  Thu, 27 Oct 2016 17:31:23 +0300

dogtag-pki (10.3.5-4) unstable; urgency=medium

  * tests: Add simple autopkgtest that runs setup-ds and pkispawn.
  * server.postinst: Tell pki-server migrate to migrate to tomcat8.
  * debian-support.diff: Fix some jar symlinks, and drop extra / from
    config dirs.
  * control: Add libcommons-collections3-java, libcommons-dbcp-java,
    libcommons-pool-java, libjboss-logging-java, libsymkey-java to pki-
    server Depends.

 -- Timo Aaltonen <tjaalton@debian.org>  Mon, 03 Oct 2016 19:30:15 +0300

dogtag-pki (10.3.5-3) unstable; urgency=medium

  * rules: Fix path to tomcat8 catalina and util jars, add api and
    util-scan jars.

 -- Timo Aaltonen <tjaalton@debian.org>  Fri, 30 Sep 2016 18:06:36 +0300

dogtag-pki (10.3.5-2) unstable; urgency=medium

  * Migrate to tomcat8.
  * server.postinst: Run pki-migrate for tomcat migration.
  * fix-cli-migrate.diff: Replace tomcat path hardcoding with ours.
  * rules: Set JAVA_HOME as a confflag so that it's added to pki.conf.
  * copyright: Updated.

 -- Timo Aaltonen <tjaalton@debian.org>  Fri, 30 Sep 2016 15:42:40 +0300

dogtag-pki (10.3.5-1) unstable; urgency=medium

  * New upstream release.
    - support-only-tomcat.diff, use-dot-instead-of-source.diff:
      dropped, upstream
    - fix-debian-paths-for-pki-cli.diff dropped, unnecessary
    - refresh patches
  * pki-tools: Add more manpages to install, DRMTool got renamed to
    KRATool so add convenience links.
  * control:
    - bump libjss-java build-dep
    - add python-nss, python-requests and python-urllib3 to build-depends
    - add python-nss and python-urllib3 to pki-base depends
    - add libjaxrs-api-java to build-depends
    - move python-ldap and python-lxml depends from pki-base to pki-server
  * rules: Fix jackson/jaxrs jar names so build finds them.
  * Split pki-base-java from -base, add python3-pki-base.
  * server.install: Simplify a bit.
  * debian-support.diff: Force bash in base/server/scripts/operations.
  * copyright: Updated.
  * {base,server}.postinst: Lintian fixes, don't use full path for
    binaries.
  * patches: Merge fix-default-settings.diff into debian-support.diff,
    and modify d-s a bit more for upstream inclusion.

 -- Timo Aaltonen <tjaalton@debian.org>  Thu, 22 Sep 2016 16:07:26 +0300

dogtag-pki (10.2.6+git20160317-2) unstable; urgency=medium

  * pki-{ca,kra,ocsp,tks,tps}.dirs: Deleted, the obsolete targets are
    long gone anyway.
  * control: Drop selinux-policy-dev from build-depends. (Closes:
    #821810)
  * pki-base.postinst: Strip extra cruft from version number for pki-
    upgrade, only the upstream base version matters. (Closes: #821909)

 -- Timo Aaltonen <tjaalton@debian.org>  Wed, 18 May 2016 10:48:27 +0300

dogtag-pki (10.2.6+git20160317-1) unstable; urgency=medium

  * update to current 10_2_6_BRANCH.
    - refresh patches
    - add pki-user-membership.1 to pki-tools
    - tomcat7-build-fix.diff: Dropped, upstream.
  * rules: Mark systemd units disabled by default.
  * use-usr-bin.diff: Updated.
  * use-root-homedir.diff: Force home_dir to be /root, so that ipa works
    right.
  * control: Add conflicts on strongswan-pki.
  * pki-server: Remove default.cfg, logs on purge. (Closes: #814636)
  * pki-base: Remove pki.conf on purge. (Closes: #804312)

 -- Timo Aaltonen <tjaalton@debian.org>  Tue, 05 Apr 2016 19:37:03 +0300

dogtag-pki (10.2.6-3) unstable; urgency=medium

  * pki-base.postrm: Remove upgrade logs on purge. (Closes: #801139)
  * use-usr-bin.diff: Fix paths to binaries to use /usr/bin instead of
    /bin.

 -- Timo Aaltonen <tjaalton@debian.org>  Fri, 08 Jan 2016 02:18:17 +0200

dogtag-pki (10.2.6-2) unstable; urgency=medium

  * pki-server.dirs: Add pki-tomcatd-nuxwdog.target.wants.
  * base.postrm: No need to remove /etc/pki here.
  * debian-support.diff: Drop /etc/default/tomcat8, was a leftover from
    t8 testing. (Closes: #800558)
  * debian-support.diff: Import /lib/lsb/init-functions in
    scripts/operations. (Closes: #800559)

 -- Timo Aaltonen <tjaalton@debian.org>  Thu, 01 Oct 2015 08:14:52 +0300

dogtag-pki (10.2.6-1) unstable; urgency=medium

  * New upstream release based on DOGTAG_10_2_6_FEDORA_22_23_20150718 tag.
  * Refresh patches, drop upstreamed ones.
  * control: Drop libcrypt-ssleay-perl and libxml-perl from depends.
  * pki-tools.install: Add pki-ca-profile manpage.
  * control: Add python-sphinx to build-depends.
  * fix-jackson-paths.diff: Dropped, obsolete. Refresh other patches
    to drop unused jackson includes.
  * pki-server.install: Add sbin/pki-server.
  * control: strongswan-starter didn't rename it's 'pki' as planned, so
    add an unversioned Conflicts to pki-tools. (Closes: #767561)
  * control: Update vcs-git location (-> dogtag-pki.git).
  * Update patches.
  * control, pki-server.install: Clean up perl stuff, drop pki-setup-
    proxy which is gone.
  * control: Add libnuxwdog-java to build-depends, and pki-server depends.
  * install: Added new manpages, nuxwdog support, html docs to pki-base.
  * debian-support.diff: Fix nuxwdog paths.
  * debian-support.diff: Don't try to manage rc3.d/* symlinks.
  * rules: Symlink jars under subsystem WEB-INF.
  * pki-server.postinst, pki-base.post{inst,rm}: Add pki-upgrade/pki-
    server-upgrade snippets.
  * rules, patches: Explicitly build using tomcat7, and add a dummy
    method. (Closes: #789138)
  * use-dot-instead-of-source.diff: Fix bashisms.
  * debian-support.diff: Fix JNI_JAR_DIR.

 -- Timo Aaltonen <tjaalton@debian.org>  Wed, 23 Sep 2015 18:29:12 +0300

dogtag-pki (10.2.0-4) unstable; urgency=medium

  * control: Add python-selinux to pki-server depends.

 -- Timo Aaltonen <tjaalton@debian.org>  Fri, 07 Nov 2014 11:08:29 +0200

dogtag-pki (10.2.0-3) unstable; urgency=medium

  * control: Add Breaks/Replaces on strongswan-starter to pki-tools.
    (Closes: #767561)

 -- Timo Aaltonen <tjaalton@debian.org>  Wed, 05 Nov 2014 00:40:10 +0200

dogtag-pki (10.2.0-2) unstable; urgency=medium

  * patches: Fix servlet jar name, and paths to jss4.jar and symkey.jar.

 -- Timo Aaltonen <tjaalton@debian.org>  Fri, 24 Oct 2014 20:39:24 +0300

dogtag-pki (10.2.0-1) unstable; urgency=low

  * Initial release (Closes: #653606)

 -- Timo Aaltonen <tjaalton@debian.org>  Fri, 10 Oct 2014 14:40:12 +0300
