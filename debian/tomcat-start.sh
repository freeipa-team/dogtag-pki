#!/bin/sh
#
# Startup script for PKI Tomcat with systemd
#

set -e

# Start Tomcat
cd $CATALINA_BASE && exec $CATALINA_HOME/bin/catalina.sh run
